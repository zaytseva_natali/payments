DROP DATABASE IF EXISTS `payments`;
CREATE DATABASE IF NOT EXISTS `payments`;
USE `payments`;
CREATE TABLE `client` (
  `id`        SERIAL NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(25),
  `lastName`  VARCHAR(25),
  `login`     VARCHAR(50),
  `password`  VARCHAR(50),
  `birthday`  DATE,
  `role`      VARCHAR(20),
  `email`     VARCHAR(70),
  PRIMARY KEY (`id`)
);

CREATE TABLE `account` (
  `id`            SERIAL NOT NULL AUTO_INCREMENT,
  `client_id`     BIGINT(20) UNSIGNED,
  `creationdate`  DATE,
  `creditcard_id` BIGINT(20) UNSIGNED,
  PRIMARY KEY (`id`)
);

CREATE TABLE `creditCard` (
  `id`             SERIAL NOT NULL AUTO_INCREMENT,
  `client_id`      BIGINT(20) UNSIGNED,
  `cardopendate`   DATE,
  `payment_id`     BIGINT(20) UNSIGNED,

  PRIMARY KEY (`id`)
);

CREATE TABLE `payment` (
  `id`               SERIAL NOT NULL AUTO_INCREMENT,
  `creditcard_id`    BIGINT(20) UNSIGNED,
  `operationname`    VARCHAR(15),
  `operationdate`    DATETIME,
  `operationdetails` VARCHAR(250),
  PRIMARY KEY (`id`)
);


