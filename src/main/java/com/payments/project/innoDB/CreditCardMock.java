package com.payments.project.innoDB;


import com.payments.project.entity.CreditCard;
import com.payments.project.entity.Payment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by natalia on 21.08.17.
 */
 class CreditCardMock {

    private static CreditCardMock creditCardMock;

    private CreditCardMock() {
    }

    static CreditCardMock getClientMockUp(){
        if(creditCardMock == null){
            synchronized (CreditCardMock.class){
                creditCardMock = new CreditCardMock();
            }
        }
        return creditCardMock;
    }

    CreditCard getSingleCreditCard(){
        ClientMockUp clientMockUp = ClientMockUp.getClientMockUp();
        return new CreditCard(clientMockUp.getSingleClient(), new HashSet<Payment>());
    }

    List<CreditCard> creditCards = new ArrayList<CreditCard>(){{
        ClientMockUp clientMockUp = ClientMockUp.getClientMockUp();
        add(new CreditCard(clientMockUp.getSingleClient(), new HashSet<Payment>()));
    }};
}
