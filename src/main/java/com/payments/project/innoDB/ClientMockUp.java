package com.payments.project.innoDB;


import com.payments.project.entity.Client;

import java.util.Date;

/**
 * Created by natalia on 21.08.17.
 */
 class ClientMockUp {

    private static ClientMockUp clientMockUp;

    private ClientMockUp() {
    }

    public static ClientMockUp getClientMockUp(){
        if(clientMockUp == null){
            synchronized (ClientMockUp.class){
                clientMockUp = new ClientMockUp();
            }
        }
        return clientMockUp;
    }

    Client getSingleClient(){
        return new Client("Ben", "Morgan", new Date(), "a", "a", "a");
    }
}
