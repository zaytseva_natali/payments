package com.payments.project.innoDB;


import com.payments.project.entity.CreditCard;
import com.payments.project.entity.OperationNames;
import com.payments.project.entity.Payment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by natalia on 22.08.17.
 */
public class PaymentMock {

    List<Payment> paymentList = new ArrayList<Payment>(){{
        CreditCard creditCard = CreditCardMock.getClientMockUp().getSingleCreditCard();
        add(new Payment(creditCard, OperationNames.INCOME, "string"));
    }};
}
