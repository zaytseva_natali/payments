package com.payments.project.innoDB;

import com.payments.project.entity.Account;
import com.payments.project.entity.Client;
import com.payments.project.entity.CreditCard;
import com.payments.project.entity.Payment;

import java.util.LinkedList;
import java.util.List;

public final class InMemoryDB<T> {
    private static InMemoryDB inMemoryDB;
    private List<Client> clients = new LinkedList<>();
    private List<CreditCard> creditCards = new LinkedList<>();
    private List<Payment> payments = new LinkedList<>();
    private List<Account> accounts = new LinkedList<>();
    private int userIdCounter = 0;
    private int creditCardIdCounter = 0;
    private int paymentsIdCounter = 0;
    private int accountIdCounter = 0;

    private InMemoryDB() {
    }

    public static InMemoryDB getInstance() {
        if (inMemoryDB == null) {
            synchronized (InMemoryDB.class) {
                if (inMemoryDB == null) {
                    inMemoryDB = new InMemoryDB();
                }
            }
        }
        return inMemoryDB;
    }

    public List<T> fetchAll(Class<T> entity) {
        List<T> result = null;
        if (entity.equals(Client.class)) {
            result = (List<T>) clients;
        } else if (entity.equals(CreditCard.class)) {
            result = (List<T>) creditCards;
        } else if (entity.equals(Payment.class)) {
            result = (List<T>) payments;
        } else if (entity.equals(Account.class)) {
            result = (List<T>) accounts;
        }
        return result;
    }

    public void create(T entity) {
        if (entity instanceof Client) {
            saveClient((Client) entity);
        } else if (entity instanceof Account) {
            addAccount((Account) entity);
        } else if (entity instanceof CreditCard) {
            addCreditCard((CreditCard) entity);
        } else if (entity instanceof Payment) {
            addPayment((Payment) entity);
        }
    }

    public void update(T entity) {
        if (entity instanceof Client) {
            updateClient((Client) entity);
        } else if (entity instanceof Account) {
            updateAccount((Account) entity);
        } else if (entity instanceof CreditCard) {
            updateCreditCard((CreditCard) entity);
        } else if (entity instanceof Payment) {
            updatePayment((Payment) entity);
        }
    }

    public void delete(T entity) {
        if (entity instanceof Client) {
            deleteClient(((Client) entity).getId());
        } else if (entity instanceof Account) {
            deleteAccount(((Account) entity).getId());
        } else if (entity instanceof CreditCard) {
            deleteCreditCard(((CreditCard) entity).getId());
        } else if (entity instanceof Payment) {
            deletePayment(((Payment) entity).getId());
        }
    }

    public T get(Class<T> entity, Integer itemId) {
        T result = null;
        if (entity.equals(Client.class)) {
            result = (T) getUserById(itemId);
        } else if (entity.equals(Account.class)) {
            result = (T) getAccountById(itemId);
        } else if (entity.equals(CreditCard.class)) {
            result = (T) getPaymentCardById(itemId);
        } else if (entity.equals(Payment.class)) {
            result = (T) getPaymentById(itemId);
        }
        return result;
    }


    //Client
    private void saveClient(Client user) {
        user.setId(userIdCounter);
        clients.add(user);
        userIdCounter++;
    }

    private Client getUserById(Integer itemId) {
        Client result = null;
        if (itemId != null && itemId < clients.size()) {
            result = clients.get(itemId);
        }
        return result;
    }

    private void updateClient(Client user) {
        Client itemToUpdate = clients.get(user.getId());
        itemToUpdate.setFirstName(user.getFirstName());
        itemToUpdate.setLastName(user.getLastName());
        itemToUpdate.setLogin(user.getLogin());
        itemToUpdate.setPassword(user.getPassword());
        itemToUpdate.setEmail(user.getEmail());
        //itemToUpdate.setRole(user.getRole());
        itemToUpdate.setBirthDate(user.getBirthDate());
    }

    private void deleteClient(Integer userId) {
        Client itemToDelete = clients.get(userId);
        if (itemToDelete != null) {
            clients.remove(itemToDelete);
        }
    }

    public Client findUserByLoginPassword(String login, String password) {
        Client result = null;
        if (!checkNull()) {
            if (clients != null) {
                for (Client user : clients) {
                    if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                        result = user;
                    }
                }
            }
        } else {
            result = null;
        }
        return result;
    }

    private boolean checkNull(Object... var) {
        boolean isNull = false;
        for (Object o : var) {
            if (!isNull) {
                isNull = o == null;
            }
        }
        return isNull;
    }

    //Account
    private void addAccount(Account account) {
        account.setId(accountIdCounter);
        accounts.add(account);
        accountIdCounter++;
    }

    private Account getAccountById(Integer itemId) {
        Account result = null;
        if (itemId != null && itemId < accounts.size()) {
            result = accounts.get(itemId);
        }
        return result;
    }

    private void updateAccount(Account account) {
        Account itemToUpdate = accounts.get(account.getId());
        itemToUpdate.setCreditCardList(account.getCreditCardList());
    }

    private void deleteAccount(Integer movieId) {
        if (movieId < accounts.size() && movieId >= 0) {
            Account itemToDelete = accounts.get(movieId);
            accounts.remove(itemToDelete);
        }
    }

    //CreditCard
    private void addCreditCard(CreditCard creditCard) {
        creditCard.setId(creditCardIdCounter);
        creditCards.add(creditCard);
        creditCardIdCounter++;
    }

    private CreditCard getPaymentById(Integer itemId) {
        CreditCard result = null;
        if (itemId != null && itemId < creditCards.size()) {
            result = creditCards.get(itemId);
        }
        return result;
    }

    private void updateCreditCard(CreditCard creditCard) {
        CreditCard itemToUpdate = creditCards.get(creditCard.getId());
        itemToUpdate.setPayments(creditCard.getPayments());
    }

    private void deleteCreditCard(int creditCardId) {
        CreditCard creditCardToDelete = creditCards.get(creditCardId);
        if (creditCardToDelete != null) {
            creditCards.remove(creditCardId);
        }
    }

    // Payment
    private void addPayment(Payment payment) {
        payment.setId(paymentsIdCounter);
        payments.add(payment);
        paymentsIdCounter++;
    }

    private Payment getPaymentCardById(Integer itemId) {
        Payment result = null;
        if (itemId != null && itemId < payments.size()) {
            result = payments.get(itemId);
        }
        return result;
    }

    private void updatePayment(Payment payment) {
        Payment itemToUpdate = payments.get(payment.getId());
        itemToUpdate.setOperationDetails(payment.getOperationDetails());
        itemToUpdate.setOperationName(payment.getOperationName());
        itemToUpdate.setCreditCard(payment.getCreditCard());
    }

    private void deletePayment(Integer paymentId) {
        Payment paymentToDelete = payments.get(paymentId);
        if (paymentToDelete != null) {
            payments.remove(paymentId);
        }
    }

}
