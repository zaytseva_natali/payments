package com.payments.project.dto;

import java.util.Date;
import java.util.Set;

/**
 * Created by natalia on 05.09.17.
 */
public class CreditCardDTO {
    private int id;
    private final ClientDTO client;
    private Date cardOpenDate;
    private Set<PaymentDTO> payments;

    public CreditCardDTO(ClientDTO client) {
        this.client = client;
    }

    public CreditCardDTO(ClientDTO client, Set<PaymentDTO> payments) {
        this.client = client;
        this.cardOpenDate = new Date();
        this.payments = payments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ClientDTO getClient() {
        return client;
    }

    public Date getCardOpenDate() {
        return cardOpenDate;
    }

    public void setCardOpenDate(Date cardOpenDate) {
        this.cardOpenDate = cardOpenDate;
    }

    public Set<PaymentDTO> getPayments() {
        return payments;
    }

    public void setPayments(Set<PaymentDTO> payments) {
        this.payments = payments;
    }
}
