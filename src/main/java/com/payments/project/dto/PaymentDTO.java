package com.payments.project.dto;

import com.payments.project.entity.OperationNames;

import java.util.Date;

/**
 * Created by natalia on 05.09.17.
 */
public class PaymentDTO {
    private int id;
    private Date operationDate;
    private CreditCardDTO creditCard;
    private OperationNames operationName;
    private String operationDetails;

    public PaymentDTO() {
    }

    public PaymentDTO(CreditCardDTO creditCard, OperationNames operationName, String operationDetails) {
        this.operationDate = new Date();
        this.creditCard = creditCard;
        this.operationName = operationName;
        this.operationDetails = operationDetails;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public CreditCardDTO getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCardDTO creditCard) {
        this.creditCard = creditCard;
    }

    public OperationNames getOperationName() {
        return operationName;
    }

    public void setOperationName(OperationNames operationName) {
        this.operationName = operationName;
    }

    public String getOperationDetails() {
        return operationDetails;
    }

    public void setOperationDetails(String operationDetails) {
        this.operationDetails = operationDetails;
    }
}
