package com.payments.project.dto;

import com.payments.project.entity.Client;
import com.payments.project.entity.CreditCard;

import java.util.Date;
import java.util.List;

/**
 * Created by natalia on 22.08.17.
 */
public class AccountDTO {
    private int id;
    private final ClientDTO client;
    private Date creationDate;
    private List<CreditCard> creditCardList;

    public AccountDTO(ClientDTO client) {
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ClientDTO getClient() {
        return client;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<CreditCard> getCreditCardList() {
        return creditCardList;
    }

    public void setCreditCardList(List<CreditCard> creditCardList) {
        this.creditCardList = creditCardList;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", client=" + client +
                ", creationDate=" + creationDate +
                ", creditCardList=" + creditCardList +
                '}';
    }
}
