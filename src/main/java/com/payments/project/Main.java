package com.payments.project;

import com.payments.project.dto.AccountDTO;
import com.payments.project.entity.Account;
import com.payments.project.entity.Client;
import com.payments.project.services.AccountService;
import com.payments.project.services.impl.AccountServiceImpl;
import com.payments.project.transformer.AccountTransformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;


public class Main {

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        AccountService accountService = new AccountServiceImpl();
        Client client1 = new Client("Ali", "Aliev", dateformat.parse("17/07/1989"), "ali", "ali", "ali@Aliev.com");
        Account account1 = new Account(client1);
        AccountDTO accountDTO1 = AccountTransformer.accountToDto(account1);

        accountService.create(accountDTO1);

        Client client2 = new Client("megan", "Fox", dateformat.parse("01/12/1988"), "megan", "megan", "megan@fox.com");
        Account account2 = new Account(client2);
        AccountDTO accountDTO2 = AccountTransformer.accountToDto(account2);

        accountService.create(accountDTO2);

        List<AccountDTO> all = accountService.getAll();
        System.out.println(all.toString());
        System.out.println("---------------------");

        AccountDTO accountByClient = accountService.getAccountByClient(client1);
        System.out.println(accountByClient);
        System.out.println("---------------------");

        AccountDTO accountByName = accountService.getAccountByName("megan", "Fox");
        System.out.println(accountByName);


    }
}
