package com.payments.project.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by natalia on 18.08.17.
 */
public class Account {

    private int id;
    private final Client client;
    private Date creationDate;
    private List<CreditCard> creditCardList;

    public Account(final Client client) {
        this.client = client;
        this.creationDate = new Date();
        this.creditCardList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<CreditCard> getCreditCardList() {
        return creditCardList;
    }

    public void setCreditCardList(List<CreditCard> creditCardList) {
        this.creditCardList = creditCardList;
    }


}
