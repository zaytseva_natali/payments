package com.payments.project.entity;

import java.util.Date;

/**
 * Created by natalia on 18.08.17.
 */
public class Payment {

    private int id;
    private Date operationDate;
    private CreditCard creditCard;
    private OperationNames operationName;
    private String operationDetails;

    public Payment() {
    }

    public Payment(CreditCard creditCard, OperationNames operationName, String operationDetails) {
        this.operationDate = new Date();
        this.creditCard = creditCard;
        this.operationName = operationName;
        this.operationDetails = operationDetails;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public OperationNames getOperationName() {
        return operationName;
    }

    public void setOperationName(OperationNames operationName) {
        this.operationName = operationName;
    }

    public String getOperationDetails() {
        return operationDetails;
    }

    public void setOperationDetails(String operationDetails) {
        this.operationDetails = operationDetails;
    }
}
