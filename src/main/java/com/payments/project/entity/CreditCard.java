package com.payments.project.entity;

import java.util.Date;
import java.util.Set;

public class CreditCard {
    private int id;
    private final Client client;
    private Date cardOpenDate;
    private Set<Payment> payments;

    public CreditCard(Client client) {
        this.client = client;
    }

    public CreditCard(Client client, Set<Payment> payments) {
        this.client = client;
        this.cardOpenDate = new Date();
        this.payments = payments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public Date getCardOpenDate() {
        return cardOpenDate;
    }

    public void setCardOpenDate(Date cardOpenDate) {
        this.cardOpenDate = cardOpenDate;
    }

    public Set<Payment> getPayments() {
        return payments;
    }

    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }
}
