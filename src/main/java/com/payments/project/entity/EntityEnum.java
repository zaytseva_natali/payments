package com.payments.project.entity;

/**
 * Created by natalia on 28.08.17.
 */
public enum EntityEnum {
    CLIENT(Client.class), ACCOUNT(Account.class), CREDITCARD(CreditCard.class), PAYMENT(Payment.class);

    private Class entityClass;

    EntityEnum(Class entityClass) {
        this.entityClass = entityClass;
    }

    public static <T> EntityEnum getValueByClass(Class<T> searchableEntity) {
        EntityEnum result = null;
        for (EntityEnum entityEnum : EntityEnum.values()) {
            if(entityEnum.entityClass.equals(searchableEntity)){
                result = entityEnum;
            }
        }
        return result;
    }

}
