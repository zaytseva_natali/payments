package com.payments.project.entity;

/**
 * Created by natalia on 28.08.17.
 */
public enum RoleEnum {
    USER, ADMIN
}
