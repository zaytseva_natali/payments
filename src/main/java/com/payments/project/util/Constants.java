package com.payments.project.util;

/**
 * Created by natalia on 26.08.17.
 */
public class Constants {
    public static final String MOVIE_ID = "movieID";
    public static final String SESSION_ID = "sessionID";
    public static final String HALL_ID = "hallID";

    //DB credentials
    public static final String USE_MYSQL = "useMYSQL";
    public static final String DRIVER = "driver";
    public static final String JDBC_URL = "jdbcUrl";
    public static final String LOGIN = "user";
    public static final String PASSWORD = "password";
    public static final String DATABASE_NAME = "payments";

    //property path
    public final static String PATH_TO_FILE = "src\\main\\resources\\application.properties";

    //DB
    public static final String SELECT_ALL = "SELECT * FROM %s";
    public static final String FIND_BY_ID = "SELECT * FROM %s WHERE id =?";
    public static final String FIND_USER_BY_PASSWORD_AND_LOGIN = "SELECT * FROM payments.user WHERE login =? AND password =?";
    public static final String FIND_MOVIE_BY_TITLE = "SELECT * FROM payments.movie WHERE title =?";
    public static final String FIND_MOVIE_BY_DESCRIPTION = "SELECT * FROM payments.movie WHERE description =?";
    public static final String FIND_MOVIES_WITH_SESSION_AND_HALL = "SELECT DISTINCT movie.id, movie.title, movie.description," +
            " movie.duration, movie.rating FROM movie" +
            " INNER JOIN  session s on s.movie_id = movie.id" +
            " INNER join hall h on  h.id = s.hall_id";
    public static final String FIND_ACCOUNT_BY_CLIENT_ID = "SELECT * FROM payments.account WHERE client_id =?";
    public static final String FIND_CREDIT_CARD_BY_CLIENT_ID = "SELECT * FROM payments.creditcard WHERE client_id =?";
    public static final String FIND_ACCOUNT_BY_FNAME_AND_LNAME = "SELECT * FROM payments.account as ac LEFT JOIN payments.client as cl ON ac.client_id=cl.id WHERE cl.firstName =? AND cl.lastName";
    public static final String FIND_SESSION_BY_MOVIE_ID = "SELECT * FROM payments.session WHERE movie_id =?";
    public static final String FIND_TICKETS_BY_SESSION_ID = "SELECT * FROM payments.ticket WHERE session_id =?";
    public static final String INSERT = "INSERT INTO %s %s";
    public static final String UPDATE = "UPDATE %s SET %s WHERE id =?";
    public static final String DELETE = "DELETE FROM %s WHERE id =?";
    public static final String SELECT_WITH_ONE_PARAM = "SELECT * FROM %s WHERE %s =?";
    public static final String SELECT_WITH_TWO_PARAM = "SELECT * FROM %s WHERE %s =? AND %s =?";


    public static final String FIND_SESSION_BY_HALL_ID_AND_MOVIE_ID = "SELECT * FROM payments.session WHERE hall_id =? AND movie_id =?";

}
