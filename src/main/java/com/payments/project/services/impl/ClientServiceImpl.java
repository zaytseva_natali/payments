package com.payments.project.services.impl;

import com.payments.project.dao.api.ClientCrudDao;
import com.payments.project.dao.impl.ClientCrudDaoImpl;
import com.payments.project.dao.impl.ClientCrudDaoInMemoryimpl;
import com.payments.project.datasource.PropertyReader;
import com.payments.project.dto.ClientDTO;
import com.payments.project.entity.Client;
import com.payments.project.services.ClientService;
import com.payments.project.transformer.ClientTransformer;

import java.util.List;

public class ClientServiceImpl implements ClientService {

    @Override
    public ClientDTO getClientByFnameAndLname(String fname, String lname) {
        ClientCrudDao clientCrudDao;
        if(!PropertyReader.getInstance().isInMemoryDB()) {
            clientCrudDao =  ClientCrudDaoInMemoryimpl.getInstance();
        }else {
            clientCrudDao = ClientCrudDaoImpl.getInstance();
        }
        Client client = clientCrudDao.getClientByFnameAndLname(fname, lname);
        ClientDTO result = null;
        if(client != null) {
            result = ClientTransformer.clientToDto(client);
        }
        return result;
    }

    @Override
    public ClientDTO getClientByEmail(String email) {
        ClientCrudDao clientCrudDao;
        if(!PropertyReader.getInstance().isInMemoryDB()) {
            clientCrudDao =  ClientCrudDaoInMemoryimpl.getInstance();
        }else {
            clientCrudDao = ClientCrudDaoImpl.getInstance();
        }
        Client client = clientCrudDao.getClientByEmail(email);
        ClientDTO result = null;
        if(client != null) {
            result = ClientTransformer.clientToDto(client);
        }
        return result;
    }

    @Override
    public List<ClientDTO> getAll() {
        ClientCrudDao clientCrudDao;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            clientCrudDao = ClientCrudDaoInMemoryimpl.getInstance();
        } else {
            clientCrudDao = ClientCrudDaoImpl.getInstance();
        }
        List<Client> clients = clientCrudDao.getAll();
        List<ClientDTO> dtos = ClientTransformer.clientListToDto(clients);
        return dtos;
    }

    @Override
    public void create(ClientDTO dto) {
        ClientCrudDao clientDao;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            clientDao = ClientCrudDaoInMemoryimpl.getInstance();
        } else {
            clientDao = ClientCrudDaoImpl.getInstance();
        }
        Client client = ClientTransformer.dtoToClient(dto);
        clientDao.create(client);
    }

    @Override
    public void update(ClientDTO dto) {
        ClientCrudDao clientDAO;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            clientDAO = ClientCrudDaoInMemoryimpl.getInstance();
        } else {
            clientDAO = ClientCrudDaoImpl.getInstance();
        }
        Client client = ClientTransformer.dtoToClient(dto);
        clientDAO.update(client);
    }

    @Override
    public void delete(Integer id) {
        ClientCrudDao clientDAO;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            clientDAO = ClientCrudDaoInMemoryimpl.getInstance();
        } else {
            clientDAO = ClientCrudDaoImpl.getInstance();
        }
        clientDAO.delete(id);
    }

    @Override
    public ClientDTO get(Integer id) {
        ClientCrudDao clientDAO;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            clientDAO = ClientCrudDaoInMemoryimpl.getInstance();
        } else {
            clientDAO = ClientCrudDaoImpl.getInstance();
        }
        Client client = clientDAO.get(id);
        return ClientTransformer.clientToDto(client);
    }
}
