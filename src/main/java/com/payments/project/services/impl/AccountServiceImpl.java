package com.payments.project.services.impl;

import com.payments.project.dao.api.AccountCrudDao;
import com.payments.project.dao.impl.AccountCrudDaoImpl;
import com.payments.project.dao.impl.AccountCrudDaoInMemoryImp;
import com.payments.project.datasource.PropertyReader;
import com.payments.project.dto.AccountDTO;
import com.payments.project.entity.Account;
import com.payments.project.entity.Client;
import com.payments.project.services.AccountService;
import com.payments.project.transformer.AccountTransformer;

import java.util.List;

/**
 * Created by natalia on 28.08.17.
 */
public class AccountServiceImpl implements AccountService{

    @Override
    public List<AccountDTO> getAll() {
        AccountCrudDao accountCrudDao;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            accountCrudDao = AccountCrudDaoInMemoryImp.getInstance();
        } else {
            accountCrudDao = AccountCrudDaoImpl.getInstance();
        }
        List<Account> accounts = accountCrudDao.getAll();
        List<AccountDTO> dtos = AccountTransformer.accountsToDto(accounts);
        return dtos;

    }

    @Override
    public void create(AccountDTO dto) {
        AccountCrudDao accountDao;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            accountDao = AccountCrudDaoInMemoryImp.getInstance();
        } else {
            accountDao = AccountCrudDaoImpl.getInstance();
        }
        Account account = AccountTransformer.dtoToAccount(dto);
        accountDao.create(account);

    }

    @Override
    public void update(AccountDTO dto) {
        AccountCrudDao accountDAO;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            accountDAO = AccountCrudDaoInMemoryImp.getInstance();
        } else {
            accountDAO = AccountCrudDaoImpl.getInstance();
        }
        Account account = AccountTransformer.dtoToAccount(dto);
        accountDAO.update(account);

    }

    @Override
    public void delete(Integer id) {
        AccountCrudDao accountDAO;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            accountDAO = AccountCrudDaoInMemoryImp.getInstance();
        } else {
            accountDAO = AccountCrudDaoImpl.getInstance();
        }
        accountDAO.delete(id);

    }

    @Override
    public AccountDTO get(Integer id) {
        AccountCrudDao accountDAO;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            accountDAO = AccountCrudDaoInMemoryImp.getInstance();
        } else {
            accountDAO = AccountCrudDaoImpl.getInstance();
        }
        Account account = accountDAO.get(id);
        return AccountTransformer.accountToDto(account);
    }

    @Override
    public AccountDTO getAccountByName(String fname, String lname) {
        AccountCrudDao accountDAO;
        if(!PropertyReader.getInstance().isInMemoryDB()) {
            accountDAO =  AccountCrudDaoInMemoryImp.getInstance();
        }else {
            accountDAO = AccountCrudDaoImpl.getInstance();
        }
        Account account = accountDAO.getAccountByFirstNameAndLastName(fname, lname);
        AccountDTO result = null;
        if(account != null) {
            result = AccountTransformer.accountToDto(account);
        }
        return result;

    }

    @Override
    public AccountDTO getAccountByClient(Client client) {
        AccountCrudDao accountDAO;
        if(!PropertyReader.getInstance().isInMemoryDB()) {
            accountDAO =  AccountCrudDaoInMemoryImp.getInstance();
        }else {
            accountDAO = AccountCrudDaoImpl.getInstance();
        }
        Account account = accountDAO.getAccountByClient(client);
        AccountDTO result = null;
        if(account != null) {
            result = AccountTransformer.accountToDto(account);
        }
        return result;
    }
}
