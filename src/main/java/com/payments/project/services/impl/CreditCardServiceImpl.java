package com.payments.project.services.impl;

import com.payments.project.dao.api.CreditCardCrudDao;
import com.payments.project.dao.impl.CreditCardCrudDaoImpl;
import com.payments.project.dao.impl.CreditCardCrudDaoInMemoryImpl;
import com.payments.project.datasource.PropertyReader;
import com.payments.project.dto.ClientDTO;
import com.payments.project.dto.CreditCardDTO;
import com.payments.project.dto.PaymentDTO;
import com.payments.project.entity.CreditCard;
import com.payments.project.services.CreditCardService;
import com.payments.project.transformer.CreditCardTransformer;

import java.util.List;

/**
 * Created by natalia on 05.09.17.
 */
public class CreditCardServiceImpl implements CreditCardService {

    @Override
    public List<CreditCardDTO> getAll() {
        CreditCardCrudDao creditCardCrudDao;
        if (!PropertyReader.getInstance().isInMemoryDB()) {
            creditCardCrudDao = CreditCardCrudDaoInMemoryImpl.getInstance();
        } else {
            creditCardCrudDao = CreditCardCrudDaoImpl.getInstance();
        }
        List<CreditCard> creditCards = creditCardCrudDao.getAll();
        List<CreditCardDTO> dtos = CreditCardTransformer.creditCardListToDto(creditCards);
        return dtos;
    }

    @Override
    public void create(CreditCardDTO type) {

    }

    @Override
    public void update(CreditCardDTO type) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public CreditCardDTO get(Integer id) {
        return null;
    }

    @Override
    public CreditCardDTO findCardByClient(ClientDTO client) {
        return null;
    }

    @Override
    public List<PaymentDTO> getAllPayments(int clientId) {
        return null;
    }
}
