package com.payments.project.services;

import com.payments.project.dao.CrudDao;
import com.payments.project.dto.ClientDTO;
import com.payments.project.dto.CreditCardDTO;
import com.payments.project.dto.PaymentDTO;

import java.util.List;

/**
 * Created by natalia on 05.09.17.
 */
public interface CreditCardService extends CrudDao<CreditCardDTO, Integer> {
    CreditCardDTO findCardByClient(ClientDTO client);
    List<PaymentDTO> getAllPayments(int clientId);
}
