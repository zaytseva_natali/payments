package com.payments.project.services;

import com.payments.project.dao.CrudDao;
import com.payments.project.dto.AccountDTO;
import com.payments.project.dto.ClientDTO;
import com.payments.project.entity.Client;

public interface ClientService extends CrudDao<ClientDTO, Integer> {
    ClientDTO getClientByFnameAndLname(String fname, String lname);
    ClientDTO getClientByEmail(String email);
}
