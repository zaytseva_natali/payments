package com.payments.project.services;


import com.payments.project.dao.CrudDao;
import com.payments.project.dto.AccountDTO;
import com.payments.project.entity.Client;

public interface AccountService extends CrudDao<AccountDTO, Integer> {

    AccountDTO getAccountByName(String fname, String lname);
    AccountDTO getAccountByClient(Client client);



}
