package com.payments.project.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {
    private static DataSource dataSource;
    private static ComboPooledDataSource connectionPool;

    private DataSource() {
        initPollConnections();
    }

    public static synchronized DataSource getInstance() {
        if (dataSource == null) {
            dataSource = new DataSource();
        }
        return dataSource;
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static void initPollConnections() {
        connectionPool = new ComboPooledDataSource();
        PropertyReader propertyHolder = PropertyReader.getInstance();
        try {
            connectionPool.setDriverClass(propertyHolder.getDbDriver());
            connectionPool.setJdbcUrl(propertyHolder.getJdbcUrl());
            connectionPool.setUser(propertyHolder.getDbUserLogin());
            connectionPool.setPassword(propertyHolder.getDbUserPassword());

            connectionPool.setMinPoolSize(5);
            connectionPool.setAcquireIncrement(1);
            connectionPool.setMaxPoolSize(100);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }

}
