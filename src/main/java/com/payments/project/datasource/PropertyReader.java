package com.payments.project.datasource;

import com.payments.project.util.Constants;

import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
    private static PropertyReader propertyReader;
    private boolean isInMemoryDB;
    private String jdbcUrl;
    private String dbUserLogin;
    private String dbUserPassword;
    private String dbDriver;

    private PropertyReader() {
        loadProperties();
    }

    private void loadProperties() {
        Properties prop = new Properties();
        try {
            prop.load(getClass().getClassLoader().getResourceAsStream("application.properties"));

            this.isInMemoryDB = Boolean.valueOf(prop.getProperty(Constants.USE_MYSQL));
            this.dbDriver = prop.getProperty(Constants.DRIVER);
            this.jdbcUrl = prop.getProperty(Constants.JDBC_URL);
            this.dbUserLogin = prop.getProperty(Constants.LOGIN);
            this.dbUserPassword = prop.getProperty(Constants.PASSWORD);

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public static PropertyReader getInstance() {
        if (propertyReader == null) {
            synchronized (PropertyReader.class) {
                if (propertyReader == null) {
                    propertyReader = new PropertyReader();
                }
            }
        }
        return propertyReader;
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public String getDbUserLogin() {
        return dbUserLogin;
    }

    public String getDbUserPassword() {
        return dbUserPassword;
    }

    public boolean isInMemoryDB() {
        return isInMemoryDB;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

}
