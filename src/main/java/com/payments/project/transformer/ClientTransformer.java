package com.payments.project.transformer;

import com.payments.project.dto.ClientDTO;
import com.payments.project.entity.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientTransformer {
    public static Client dtoToClient(ClientDTO dto) {
        Client client = new Client();
        client.setFirstName(dto.getFirstName());
        client.setLastName(dto.getLastName());
        client.setLogin(dto.getLogin());
        client.setPassword(dto.getPassword());
        client.setEmail(dto.getEmail());
        client.setBirthDate(dto.getBirthDate());
        client.setRole(dto.getRole());
        client.setAccount(AccountTransformer.dtoToAccount(dto.getAccount()));
        return client;
    }

    public static ClientDTO clientToDto(Client client) {
        ClientDTO clientDto = new ClientDTO();
        clientDto.setFirstName(client.getFirstName());
        clientDto.setLastName(client.getLastName());
        clientDto.setLogin(client.getLogin());
        clientDto.setPassword(client.getPassword());
        clientDto.setEmail(client.getEmail());
        clientDto.setBirthDate(client.getBirthDate());
        clientDto.setRole(client.getRole());
        clientDto.setAccount(AccountTransformer.accountToDto(client.getAccount()));
        return clientDto;
    }

    public static List<ClientDTO> clientListToDto(List<Client> clientList) {
        List<ClientDTO> clientDTOs = new ArrayList<>();
        for (Client client : clientList) {
            ClientDTO accountDTO = clientToDto(client);
            clientDTOs.add(accountDTO);
        }
        return clientDTOs;
    }
}
