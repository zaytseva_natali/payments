package com.payments.project.transformer;

import com.payments.project.dto.PaymentDTO;
import com.payments.project.entity.Payment;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by natalia on 05.09.17.
 */
public class PaymentTransformer {

    public static Payment dtoToPayment(PaymentDTO dto) {
        Payment payment = new Payment();
        payment.setId(dto.getId());
        payment.setOperationDate(dto.getOperationDate());
        payment.setOperationDetails(dto.getOperationDetails());
        payment.setOperationName(dto.getOperationName());
        payment.setCreditCard(CreditCardTransformer.dtoToCreditCard(dto.getCreditCard()));
        return payment;
    }

    public static PaymentDTO paymentToDto(Payment payment) {
        PaymentDTO dto = new PaymentDTO();
        dto.setId(payment.getId());
        dto.setOperationDate(payment.getOperationDate());
        dto.setOperationDetails(payment.getOperationDetails());
        dto.setOperationName(payment.getOperationName());
        dto.setCreditCard(CreditCardTransformer.creditCardToDto(payment.getCreditCard()));
        return dto;
    }

    public static Set<PaymentDTO> paymentListToDto(Set<Payment> paymentList) {
        Set<PaymentDTO> paymentDTOs = new HashSet<>();
        for (Payment payment : paymentList) {
            PaymentDTO paymentDTO = paymentToDto(payment);
            paymentDTOs.add(paymentDTO);
        }
        return paymentDTOs;
    }
    public static Set<Payment> dtoListToPayments(Set<PaymentDTO> paymentDTOs) {
        Set<Payment> paymentList = new HashSet<>();
        for (PaymentDTO dto : paymentDTOs) {
            Payment payment = dtoToPayment(dto);
            paymentList.add(payment);
        }
        return paymentList;
    }
}
