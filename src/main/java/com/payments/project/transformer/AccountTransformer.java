package com.payments.project.transformer;

import com.payments.project.dto.AccountDTO;
import com.payments.project.entity.Account;
import com.payments.project.entity.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by natalia on 28.08.17.
 */
public class AccountTransformer {
    public static Account dtoToAccount(AccountDTO dto) {
        Client client = ClientTransformer.dtoToClient(dto.getClient());
        Account account = new Account(client);
        account.setCreationDate(dto.getCreationDate());
        account.setCreditCardList(dto.getCreditCardList());
        return account;
    }

    public static AccountDTO accountToDto(Account account) {
        Client client = account.getClient();
        AccountDTO accountDTO = new AccountDTO(ClientTransformer.clientToDto(client));
        accountDTO.setCreationDate(account.getCreationDate());
        accountDTO.setCreditCardList(account.getCreditCardList());
        return accountDTO;
    }

    public static List<AccountDTO> accountsToDto(List<Account> accounts) {
        List<AccountDTO> accountDTOs = new ArrayList<>();
        for (Account account : accounts) {
            AccountDTO accountDTO = accountToDto(account);
            accountDTOs.add(accountDTO);
        }
        return accountDTOs;
    }
}
