package com.payments.project.transformer;

import com.payments.project.dto.CreditCardDTO;
import com.payments.project.entity.CreditCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by natalia on 05.09.17.
 */
public class CreditCardTransformer {

    public static CreditCard dtoToCreditCard(CreditCardDTO dto) {
        CreditCard client = new CreditCard(ClientTransformer.dtoToClient(dto.getClient()));
        client.setId(dto.getId());
        client.setCardOpenDate(dto.getCardOpenDate());
        client.setPayments(PaymentTransformer.dtoListToPayments(dto.getPayments()));
        return client;
    }

    public static CreditCardDTO creditCardToDto(CreditCard creditCard) {
        CreditCardDTO dto = new CreditCardDTO(ClientTransformer.clientToDto(creditCard.getClient()));
        dto.setId(creditCard.getId());
        dto.setCardOpenDate(creditCard.getCardOpenDate());
        dto.setPayments(PaymentTransformer.paymentListToDto(creditCard.getPayments()));
        return dto;
    }

    public static List<CreditCardDTO> creditCardListToDto(List<CreditCard> creditCards) {
        List<CreditCardDTO> creditCardDTOs = new ArrayList<>();
        for (CreditCard creditCard : creditCards) {
            CreditCardDTO accountDTO = creditCardToDto(creditCard);
            creditCardDTOs.add(accountDTO);
        }
        return creditCardDTOs;
    }
}
