package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.CreditCardCrudDao;
import com.payments.project.entity.Client;
import com.payments.project.entity.CreditCard;
import com.payments.project.entity.Payment;
import com.payments.project.innoDB.InMemoryDB;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by natalia on 05.09.17.
 */
public class CreditCardCrudDaoInMemoryImpl extends DefaultCrudDao<CreditCard, Integer> implements CreditCardCrudDao {

    private static CreditCardCrudDaoInMemoryImpl creditCArdInMemoryImpl;

    public CreditCardCrudDaoInMemoryImpl(Class<CreditCard> entityClass) {
        super(entityClass);
    }

    public static CreditCardCrudDaoInMemoryImpl getInstance() {
        if (creditCArdInMemoryImpl == null) {
            synchronized (CreditCardCrudDaoInMemoryImpl.class) {
                creditCArdInMemoryImpl = new CreditCardCrudDaoInMemoryImpl(CreditCard.class);
            }
        }
        return creditCArdInMemoryImpl;
    }

    @Override
    public CreditCard findCardByClient(Client client) {
        List<CreditCard> all = getAll();
        for (CreditCard creditCard : all) {
            if (creditCard.getId() == client.getId()) {
                return creditCard;
            }
        }
        return null;
    }

    @Override
    public List<Payment> getAllPayments(int clientId) {

        CreditCard creditCard = get(clientId);
        Set<Payment> payments = creditCard.getPayments();
        return payments == null || payments.isEmpty() ? new ArrayList<Payment>() : new ArrayList<>(payments);
    }

    @Override
    public List<CreditCard> getAll() {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        List<CreditCard> accounts = inMemoryDB.fetchAll(CreditCard.class);
        return accounts;
    }

    @Override
    public void create(CreditCard account) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.create(account);

    }

    @Override
    public void update(CreditCard account) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.update(account);
    }

    @Override
    public void delete(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.delete(id);
    }

    @Override
    public CreditCard get(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        return (CreditCard) inMemoryDB.get(CreditCard.class, id);
    }
}
