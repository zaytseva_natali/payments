package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.ClientCrudDao;
import com.payments.project.entity.Client;
import com.payments.project.innoDB.InMemoryDB;

import java.util.List;

public class ClientCrudDaoInMemoryimpl extends DefaultCrudDao<Client, Integer> implements ClientCrudDao {

    public ClientCrudDaoInMemoryimpl(Class<Client> entityClass) {
        super(entityClass);
    }

    private static ClientCrudDaoInMemoryimpl clientCrudDaoInMemoryimpl;


    public static ClientCrudDaoInMemoryimpl getInstance() {
        if (clientCrudDaoInMemoryimpl == null) {
            synchronized (AccountCrudDaoInMemoryImp.class) {
                clientCrudDaoInMemoryimpl = new ClientCrudDaoInMemoryimpl(Client.class);
            }
        }
        return clientCrudDaoInMemoryimpl;
    }

    @Override
    public Client getClientByFnameAndLname(String fname, String lname) {
        List<Client> all = getAll();
        for (Client client : all) {
            if (client.getFirstName().equals(fname) && client.getLastName().equals(lname)) {
                return client;
            }
        }
        return null;
    }

    @Override
    public Client getClientByEmail(String email) {
        List<Client> all = getAll();
        for (Client client : all) {
            if (email.equals(client.getEmail())) {
                return client;
            }

        }
        return null;
    }

    @Override
    public List<Client> getAll() {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        List<Client> accounts = inMemoryDB.fetchAll(Client.class);
        return accounts;
    }

    @Override
    public void create(Client client) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.create(client);
    }

    @Override
    public void update(Client client) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.update(client);
    }

    @Override
    public void delete(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.delete(id);
    }

    @Override
    public Client get(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        return (Client) inMemoryDB.get(Client.class, id);
    }
}
