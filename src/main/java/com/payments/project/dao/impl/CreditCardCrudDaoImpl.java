package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.CreditCardCrudDao;
import com.payments.project.entity.Client;
import com.payments.project.entity.CreditCard;
import com.payments.project.entity.Payment;
import com.payments.project.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by natalia on 05.09.17.
 */
public class CreditCardCrudDaoImpl extends DefaultCrudDao<CreditCard, Integer> implements CreditCardCrudDao {

    private static CreditCardCrudDaoImpl creditCardCrudDAo;

    public CreditCardCrudDaoImpl(Class<CreditCard> entityClass) {
        super(entityClass);
    }

    public static CreditCardCrudDaoImpl getInstance() {

        if (creditCardCrudDAo == null) {
            synchronized (CreditCardCrudDaoImpl.class) {
                creditCardCrudDAo = new CreditCardCrudDaoImpl(CreditCard.class);
            }
        }
        return creditCardCrudDAo;
    }

    @Override
    public CreditCard findCardByClient(Client client) {
        Connection connection = dataSource.getConnection();
        CreditCard result = null;
        try {
            PreparedStatement preparedStatement = createSelectStatement(connection, client.getId());
            ResultSet clientRS = preparedStatement.executeQuery();
            clientRS.next();
            result = mapCreditCard(clientRS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Payment> getAllPayments(int clientId) {
        Connection connection = dataSource.getConnection();
        List<Payment> result = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = createSelectStatement(connection, "creditcard_id", clientId);
            ResultSet clientRS = preparedStatement.executeQuery();
            while (clientRS.next()) {
                Payment payment = mapPayment(clientRS);
                result.add(payment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private PreparedStatement createSelectStatement(Connection connection, String paramName, int param) throws SQLException {
        PreparedStatement preparedStatement;
        String clientSQL = String.format(Constants.SELECT_WITH_ONE_PARAM, Constants.DATABASE_NAME + "." + "payment", paramName);
        preparedStatement = connection.prepareStatement(clientSQL);
        preparedStatement.setInt(1, param);

        return preparedStatement;
    }

    private PreparedStatement createSelectStatement(Connection connection, int client_id) throws SQLException {
        PreparedStatement preparedStatement;

        String accountSQL = String.format(Constants.FIND_CREDIT_CARD_BY_CLIENT_ID);
        preparedStatement = connection.prepareStatement(accountSQL);
        preparedStatement.setInt(1, client_id);

        return preparedStatement;
    }
}
