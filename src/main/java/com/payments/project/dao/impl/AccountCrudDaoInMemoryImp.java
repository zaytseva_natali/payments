package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.AccountCrudDao;
import com.payments.project.entity.Account;
import com.payments.project.entity.Client;
import com.payments.project.innoDB.InMemoryDB;

import java.util.List;

/**
 * Created by natalia on 26.08.17.
 */
public class AccountCrudDaoInMemoryImp extends DefaultCrudDao<Account, Integer> implements AccountCrudDao {

    private static AccountCrudDaoInMemoryImp accountCrudDaoInMemoryImp;

    public AccountCrudDaoInMemoryImp(Class<Account> entityClass) {
        super(entityClass);
    }

    public static AccountCrudDaoInMemoryImp getInstance() {
        if (accountCrudDaoInMemoryImp == null) {
            synchronized (AccountCrudDaoInMemoryImp.class) {
                accountCrudDaoInMemoryImp = new AccountCrudDaoInMemoryImp(Account.class);
            }
        }
        return accountCrudDaoInMemoryImp;
    }


    @Override
    public Account getAccountByFirstNameAndLastName(String fname, String lname) {
        List<Account> all = getAll();
        for (Account account : all) {
            Client client = account.getClient();
            if(client.getFirstName().equals(fname) && client.getLastName().equals(lname)){
                return account;
            }
        }
        return null;
    }

    @Override
    public Account getAccountByClient(Client client) {
        List<Account> all = getAll();
        for (Account account : all) {
            if(account.getClient().equals(client)){
                return account;
            }

        }
        return null;
    }

    @Override
    public List<Account> getAll() {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        List<Account> accounts = inMemoryDB.fetchAll(Account.class);
        return accounts;

    }

    @Override
    public void create(Account account) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.create(account);

    }

    @Override
    public void update(Account account) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.update(account);

    }

    @Override
    public void delete(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.delete(id);

    }

    @Override
    public Account get(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        return (Account) inMemoryDB.get(Account.class, id);
    }

}
