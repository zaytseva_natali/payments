package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.PaymentCrudDao;
import com.payments.project.entity.OperationNames;
import com.payments.project.entity.Payment;
import com.payments.project.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by natalia on 06.09.17.
 */
public class PaymentCrudDaoImpl extends DefaultCrudDao<Payment, Integer> implements PaymentCrudDao {

    private static PaymentCrudDaoImpl paymentCrudDao;

    public PaymentCrudDaoImpl(Class<Payment> entityClass) {
        super(entityClass);
    }

    public static PaymentCrudDaoImpl getInstance() {

        if (paymentCrudDao == null) {
            synchronized (PaymentCrudDaoImpl.class) {
                paymentCrudDao = new PaymentCrudDaoImpl(Payment.class);
            }
        }
        return paymentCrudDao;
    }

    @Override
    public List<Payment> getPaymentsByOperationDate(Date date) {
        List<Payment> paymentList = new ArrayList<>();
        Connection connection = dataSource.getConnection();
        try {
            PreparedStatement preparedStatement = createSelectStatement(connection, "operationdate", date);
            ResultSet paymentRS = preparedStatement.executeQuery();
            while(paymentRS.next()) {
                Payment payment = mapPayment(paymentRS);
                paymentList.add(payment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return paymentList;
    }

    @Override
    public List<Payment> getPaymentsByOperationName(OperationNames operationName) {
        List<Payment> paymentList = new ArrayList<>();
        Connection connection = dataSource.getConnection();
        try {
            PreparedStatement preparedStatement = createSelectStatement(connection, "operationname", operationName);
            ResultSet paymentRS = preparedStatement.executeQuery();
            while(paymentRS.next()) {
                Payment payment = mapPayment(paymentRS);
                paymentList.add(payment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return paymentList;
    }

    private PreparedStatement createSelectStatement(Connection connection, String paramName, Date param) throws SQLException {
        PreparedStatement preparedStatement;
        String clientSQL = String.format(Constants.SELECT_WITH_ONE_PARAM, Constants.DATABASE_NAME + "." + "payment", paramName);
        preparedStatement = connection.prepareStatement(clientSQL);
        preparedStatement.setDate(1, new java.sql.Date(param.getTime()));

        return preparedStatement;
    }
    private PreparedStatement createSelectStatement(Connection connection, String paramName, OperationNames param) throws SQLException {
        PreparedStatement preparedStatement;
        String clientSQL = String.format(Constants.SELECT_WITH_ONE_PARAM, Constants.DATABASE_NAME + "." + "payment", paramName);
        preparedStatement = connection.prepareStatement(clientSQL);
        preparedStatement.setString(1, param.name());

        return preparedStatement;
    }
}
