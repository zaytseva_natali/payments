package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.AccountCrudDao;
import com.payments.project.entity.Account;
import com.payments.project.entity.Client;
import com.payments.project.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by natalia on 22.08.17.
 */
public class AccountCrudDaoImpl extends DefaultCrudDao<Account, Integer> implements AccountCrudDao {

    private static AccountCrudDaoImpl accountCrudDao;

    public AccountCrudDaoImpl(Class<Account> entityClass) {
        super(entityClass);
    }


    public static AccountCrudDaoImpl getInstance(){
        if(accountCrudDao == null){
            synchronized (AccountCrudDaoImpl.class){
                accountCrudDao = new AccountCrudDaoImpl(Account.class);
            }
        }
        return accountCrudDao;
    }

    @Override
    public Account getAccountByFirstNameAndLastName(String fname, String lname) {
        Connection connection = dataSource.getConnection();
        Account result = null;
        try {
            PreparedStatement preparedStatement = createSelectStatement(connection, fname, lname);
            ResultSet accountRS = preparedStatement.executeQuery();
            accountRS.next();
            result = mapAccount(accountRS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Account getAccountByClient(Client client) {

    Connection connection = dataSource.getConnection();
    Account result = null;
		try {
        PreparedStatement preparedStatement = createSelectStatement(connection, client.getId());
        ResultSet accountRS = preparedStatement.executeQuery();
        accountRS.next();
        result = mapAccount(accountRS);
    } catch (SQLException e) {
        e.printStackTrace();
    }
		return result;
}

    private PreparedStatement createSelectStatement(Connection connection, int client_id) throws SQLException {
        PreparedStatement preparedStatement;

        String accountSQL = String.format(Constants.FIND_ACCOUNT_BY_CLIENT_ID);
        preparedStatement = connection.prepareStatement(accountSQL);
        preparedStatement.setInt(1, client_id);

        return preparedStatement;
    }
    private PreparedStatement createSelectStatement(Connection connection, String fname, String lname) throws SQLException {
        PreparedStatement preparedStatement;

        String accountSQL = String.format(Constants.FIND_ACCOUNT_BY_FNAME_AND_LNAME);
        preparedStatement = connection.prepareStatement(accountSQL);
        preparedStatement.setString(1, fname);
        preparedStatement.setString(2, lname);

        return preparedStatement;
    }

}
