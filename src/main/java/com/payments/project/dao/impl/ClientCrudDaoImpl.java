package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.ClientCrudDao;
import com.payments.project.entity.Client;
import com.payments.project.util.Constants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by natalia on 01.09.17.
 */
public class ClientCrudDaoImpl extends DefaultCrudDao<Client, Integer> implements ClientCrudDao {

    private static ClientCrudDaoImpl clientCrudDao;

    public ClientCrudDaoImpl(Class<Client> entityClass) {
        super(entityClass);
    }

    public static ClientCrudDaoImpl getInstance() {
        if (clientCrudDao == null) {
            synchronized (ClientCrudDaoImpl.class) {
                clientCrudDao = new ClientCrudDaoImpl(Client.class);
            }
        }
        return clientCrudDao;
    }

    @Override
    public Client getClientByFnameAndLname(String fname, String lname) {
        Connection connection = dataSource.getConnection();
        Client result = null;
        try {
            PreparedStatement preparedStatement = createSelectStatement(connection, "firstName", fname, "lastName", lname);
            ResultSet clientRS = preparedStatement.executeQuery();
            clientRS.next();
            result = mapClient(clientRS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Client getClientByEmail(String email) {
        Connection connection = dataSource.getConnection();
        Client result = null;
        try {
            PreparedStatement preparedStatement = createSelectStatement(connection, "email", email);
            ResultSet clientRS = preparedStatement.executeQuery();
            clientRS.next();
            result = mapClient(clientRS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private PreparedStatement createSelectStatement(Connection connection, String paramName, String param) throws SQLException {
        PreparedStatement preparedStatement;
        String clientSQL = String.format(Constants.SELECT_WITH_ONE_PARAM, Constants.DATABASE_NAME + "." + "client", paramName);
        preparedStatement = connection.prepareStatement(clientSQL);
        preparedStatement.setString(1, param);

        return preparedStatement;
    }

    private PreparedStatement createSelectStatement(Connection connection, String paramName1, String param1, String paramName2, String param2) throws SQLException {
        PreparedStatement preparedStatement;
        String clientSQL = String.format(Constants.SELECT_WITH_TWO_PARAM, Constants.DATABASE_NAME + "." + "client", paramName1, paramName2);
        preparedStatement = connection.prepareStatement(clientSQL);
        preparedStatement.setString(1, param1);
        preparedStatement.setString(2, param2);

        return preparedStatement;
    }
}
