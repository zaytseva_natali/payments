package com.payments.project.dao.impl;

import com.payments.project.dao.DefaultCrudDao;
import com.payments.project.dao.api.PaymentCrudDao;
import com.payments.project.entity.OperationNames;
import com.payments.project.entity.Payment;
import com.payments.project.innoDB.InMemoryDB;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by natalia on 06.09.17.
 */
public class PaymentCrudDaoInMemoryImpl  extends DefaultCrudDao<Payment, Integer> implements PaymentCrudDao {

    private static PaymentCrudDaoInMemoryImpl paymentCrudDaoInMemory;

    public PaymentCrudDaoInMemoryImpl(Class<Payment> entityClass) {
        super(entityClass);
    }

    public static PaymentCrudDaoInMemoryImpl getInstance() {
        if (paymentCrudDaoInMemory == null) {
            synchronized (PaymentCrudDaoInMemoryImpl.class) {
                paymentCrudDaoInMemory = new PaymentCrudDaoInMemoryImpl(Payment.class);
            }
        }
        return paymentCrudDaoInMemory;
    }

    @Override
    public List<Payment> getPaymentsByOperationDate(Date date) {
        List<Payment> paymentList = getAll();
        List<Payment> result = new ArrayList<>();
        for (Payment payment : paymentList) {
            if (payment.getOperationDate().equals(date)){
                result.add(payment);
            }
        }
        return result;
    }

    @Override
    public List<Payment> getPaymentsByOperationName(OperationNames operationName) {
        List<Payment> paymentList = getAll();
        List<Payment> result = new ArrayList<>();
        for (Payment payment : paymentList) {
            if (payment.getOperationName().name().equals(operationName.name())){
                result.add(payment);
            }
        }
        return result;
    }

    @Override
    public List<Payment> getAll() {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        List<Payment> paymentList = inMemoryDB.fetchAll(Payment.class);
        return paymentList;
    }

    @Override
    public void create(Payment payment) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.create(payment);
    }

    @Override
    public void update(Payment payment) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.update(payment);
    }

    @Override
    public void delete(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        inMemoryDB.delete(id);
    }

    @Override
    public Payment get(Integer id) {
        InMemoryDB inMemoryDB = InMemoryDB.getInstance();
        return (Payment) inMemoryDB.get(Payment.class, id);
    }
}
