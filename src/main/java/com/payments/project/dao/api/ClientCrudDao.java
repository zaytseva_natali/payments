package com.payments.project.dao.api;

import com.payments.project.dao.CrudDao;
import com.payments.project.entity.Client;

/**
 * Created by natalia on 01.09.17.
 */
public interface ClientCrudDao extends CrudDao<Client,Integer> {
    Client getClientByFnameAndLname(String fname, String lname);
    Client getClientByEmail(String email);
}
