package com.payments.project.dao.api;

import com.payments.project.dao.CrudDao;
import com.payments.project.entity.Account;
import com.payments.project.entity.Client;

/**
 * Created by natalia on 22.08.17.
 */
public interface AccountCrudDao extends CrudDao<Account, Integer> {

    Account getAccountByFirstNameAndLastName(String fname, String lname);
    Account getAccountByClient(Client client);
}
