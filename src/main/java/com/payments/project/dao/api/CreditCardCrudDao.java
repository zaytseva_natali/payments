package com.payments.project.dao.api;

import com.payments.project.dao.CrudDao;
import com.payments.project.entity.Client;
import com.payments.project.entity.CreditCard;
import com.payments.project.entity.Payment;

import java.util.List;

public interface CreditCardCrudDao extends CrudDao<CreditCard,Integer> {

    CreditCard findCardByClient(Client client);
    List<Payment> getAllPayments(int clientId);
}
