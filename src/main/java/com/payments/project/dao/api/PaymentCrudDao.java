package com.payments.project.dao.api;

import com.payments.project.dao.CrudDao;
import com.payments.project.entity.OperationNames;
import com.payments.project.entity.Payment;

import java.util.Date;
import java.util.List;

/**
 * Created by natalia on 06.09.17.
 */
public interface PaymentCrudDao extends CrudDao<Payment,Integer> {

    List<Payment> getPaymentsByOperationDate(Date date);
    List<Payment> getPaymentsByOperationName(OperationNames operationName);
}
