package com.payments.project.dao;

import com.payments.project.datasource.DataSource;
import com.payments.project.entity.*;
import com.payments.project.util.Constants;

import java.sql.*;
import java.util.*;

/**
 * Created by natalia on 22.08.17.
 */
public abstract class DefaultCrudDao<T, S> implements CrudDao<T, S> {

    private Class<T> entityClass;
    protected DataSource dataSource;
    protected static Map<Class, EntityEnum> stringMap = new HashMap<>();

    public DefaultCrudDao(Class<T> entityClass) {
        this.entityClass = entityClass;
        dataSource = DataSource.getInstance();
        stringMap.put(entityClass, EntityEnum.getValueByClass(entityClass));
    }

    @Override
    public List<T> getAll() {
        String sql = String.format(Constants.SELECT_ALL, "payments." + stringMap.get(entityClass));
        Connection connection = dataSource.getConnection();
        List result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    @Override
    public void create(T type) {
        Connection connection = dataSource.getConnection();
        try {
            createInsertStatement(connection, type);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void update(T type) {
        Connection connection = dataSource.getConnection();
        try {
            PreparedStatement preparedStatement = createUpdateStatement(connection, type);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void delete(S id) {
        Connection connection = dataSource.getConnection();
        try {
            createDeleteStatement(connection, id);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public T get(S id) {
        String sql = String.format(Constants.FIND_BY_ID, stringMap.get(entityClass));
        Connection connection = dataSource.getConnection();
        T entity = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, (Integer) id);
            ResultSet resultSet = preparedStatement.executeQuery();
            entity = readOne(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return entity;

    }

    public Class<T> getEntityClass() {
        return entityClass;
    }

    private void createInsertStatement(Connection connection, T entity) throws SQLException {
        PreparedStatement preparedStatement = null;
//
//        switch (stringMap.get(entityClass)) {
//            case CLIENT:
//                Client client = (Client) entity;
//                //todo
//                String clientSQL = String.format(Constants.INSERT, "payments." + stringMap.get(entityClass), " (firstName, lastName, login, password, birthday, role, email) values (?,?,?,?,?,?,?)");
//                preparedStatement = connection.prepareStatement(clientSQL, Statement.RETURN_GENERATED_KEYS);
//                preparedStatement.setString(1, client.getFirstName());
//                preparedStatement.setString(2, client.getLastName());
//                preparedStatement.setString(3, client.getLogin());
//                preparedStatement.setString(4, client.getPassword());
//                preparedStatement.setDate(5, new java.sql.Date(client.getBirthDate().getTime());
//                preparedStatement.setString(6, client.getRole().name());
//                preparedStatement.setString(7, client.getEmail());
//                preparedStatement.executeUpdate();
//                ResultSet movieRS = preparedStatement.getGeneratedKeys();
//                movieRS.next();
//                client.setId(movieRS.getInt(1));
//                break;
//            case ACCOUNT:
//                Account account = (Account) entity;
//                if(account.getClient().getId() == -1) {
//                    saveClient(account.getClient());
//                }
//                List<CreditCard> creditCardList = account.getCreditCardList();
//                if(creditCardList.size() == 0){
//                    for (CreditCard creditCard : creditCardList) {
//                        //// TODO: 28.08.17
//                    }
//                }
//                String accountSQL = String.format(Constants.INSERT, "payments." + stringMap.get(entityClass), " (client_id, creationdate, creditcard_id) values (?,?,?,?)");
//                preparedStatement = connection.prepareStatement(accountSQL, Statement.RETURN_GENERATED_KEYS);
//                preparedStatement.setInt(1, account.getClient().getId());
//                preparedStatement.setDate(2, new java.sql.Date(account.getCreationDate().getTime());
//                preparedStatement.setString(3, account.getCreditCardList());
//                preparedStatement.executeUpdate();
//                ResultSet userRS = preparedStatement.getGeneratedKeys();
//                userRS.next();
//                account.setId(userRS.getInt(1));
//                break;
//            case CREDITCARD:
//                CreditCard creditCard = (CreditCard) entity;
//                String creditCardSQL = String.format(Constants.INSERT,  "payments." + stringMap.get(entityClass), " (client_id, cardopendate, payment_id) values (?,?,?)");
//                preparedStatement = connection.prepareStatement(creditCardSQL, Statement.RETURN_GENERATED_KEYS);
//                preparedStatement.setInt(1, creditCard.getClient().getId());
//                preparedStatement.setInt(2, new java.sql.Date(creditCard.getCardOpenDate().getTime());
//                preparedStatement.setInt(3, creditCard.getColumnCount());
//                preparedStatement.executeUpdate();
//                ResultSet hallRS = preparedStatement.getGeneratedKeys();
//                hallRS.next();
//                creditCard.setId(hallRS.getInt(1));
//                break;
//            case PAYMENT:
//                Payment payment = (Payment) entity;
//                if(payment.getMovie().getId() == -1) {
//                    saveClient(payment.getMovie());
//                }
//                if(payment.getHall().getId() == -1) {
//                    saveCreditCard(payment.getHall());
//                }
//                String paymentSQL = String.format(Constants.INSERT, "payments." + stringMap.get(entityClass), " (creditcard_id, operationname, operationdate, operationdetails) values (?,?,?,?)");
//                preparedStatement = connection.prepareStatement(paymentSQL, Statement.RETURN_GENERATED_KEYS);
//                preparedStatement.setInt(1, payment.getMovie().getId());
//                preparedStatement.setInt(2, payment.getHall().getId());
//                preparedStatement.setTimestamp(3, new java.sql.Timestamp(payment.getTime().getTime()));
//                preparedStatement.executeUpdate();
//                ResultSet sessionRS = preparedStatement.getGeneratedKeys();
//                sessionRS.next();
//                payment.setId(sessionRS.getInt(1));
//                break;
//        }
//
    }


    private PreparedStatement createUpdateStatement(Connection connection, T entity) throws SQLException {
        PreparedStatement preparedStatement = null;
//
//        switch (stringMap.get(entityClass)) {
//            case CLIENT:
//                Client movie = (Client) entity;
//                String movieSQL = String.format(Constants.UPDATE, "payments." + stringMap.get(entityClass), " title=?, description=?, duration=?, rating=?");
//                preparedStatement = connection.prepareStatement(movieSQL);
//                preparedStatement.setString(1, movie.getTitle());
//                preparedStatement.setString(2, movie.getDescription());
//                preparedStatement.setLong(3, movie.getDuration());
//                preparedStatement.setDouble(4, movie.getRating());
//                preparedStatement.setInt(5, movie.getId());
//                break;
//            case ACCOUNT:
//                Account user = (Account) entity;
//                String userSQL = String.format(Constants.UPDATE, "payments." + stringMap.get(entityClass), " firstName=?, lastName=?, login=?, password=?, birthday=?, role=?, email=?");
//                preparedStatement = connection.prepareStatement(userSQL);
//                preparedStatement.setString(1, user.getFirstName());
//                preparedStatement.setString(2, user.getLastName());
//                preparedStatement.setString(3, user.getLogin());
//                preparedStatement.setString(4, user.getPassword());
//                preparedStatement.setDate(5, new java.sql.Date(user.getBirthday().getTime()));
//                preparedStatement.setString(6, user.getRole().name());
//                preparedStatement.setString(7, user.getEmail());
//                preparedStatement.setInt(8, user.getId());
//                break;
//            case CREDITCARD:
//                CreditCard hall = (CreditCard) entity;
//                String hallSQL = String.format(Constants.UPDATE, "payments." + stringMap.get(entityClass), " name=?, rowCount=?, columnCount=?, size=?");
//                preparedStatement = connection.prepareStatement(hallSQL);
//                preparedStatement.setString(1, hall.getName());
//                preparedStatement.setInt(2, hall.getRowCount());
//                preparedStatement.setInt(3, hall.getColumnCount());
//                preparedStatement.setInt(4, hall.getSize());
//                preparedStatement.setInt(5, hall.getId());
//                break;
//            case PAYMENT:
//                Payment session = (Payment) entity;
//                if(session.getMovie().getId() == -1) {
//                    saveClient(session.getMovie());
//                }
//                if(session.getHall().getId() == -1) {
//                    saveCreditCard(session.getHall());
//                }
//                String sessionSQL = String.format(Constants.UPDATE, "payments." + stringMap.get(entityClass), " movie_id=?, hall_id=?, time=?");
//                preparedStatement = connection.prepareStatement(sessionSQL, Statement.RETURN_GENERATED_KEYS);
//                preparedStatement.setInt(1, session.getMovie().getId());
//                preparedStatement.setInt(2, session.getHall().getId());
//                preparedStatement.setTimestamp(3, new java.sql.Timestamp(session.getTime().getTime()));
//                preparedStatement.setInt(4, session.getId());
//                break;
//        }

        return preparedStatement;
    }

    private void createDeleteStatement(Connection connection, S id) throws SQLException {
        PreparedStatement preparedStatement;

        switch (stringMap.get(entityClass)) {
            case CLIENT:

                preparedStatement = connection.prepareStatement("DELETE FROM ticket where session_id=" + "ANY (SELECT session.id FROM session WHERE movie_id=?);");
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement("DELETE FROM session where movie_id=?");
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();

                String movieSQL = String.format(Constants.DELETE, "payments." + stringMap.get(entityClass));
                preparedStatement = connection.prepareStatement(movieSQL);
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();
                break;

            case ACCOUNT:

                String userSQL = String.format(Constants.DELETE, "payments." + stringMap.get(entityClass));
                preparedStatement = connection.prepareStatement(userSQL);
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();
                break;

            case CREDITCARD:
                preparedStatement = connection.prepareStatement("DELETE FROM creditcard where creditcard_id=" + " ANY (SELECT session.id FROM session WHERE hall_id=?);");
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();

                preparedStatement = connection.prepareStatement("DELETE FROM session where hall_id=?");
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();

                String hallSQL = String.format(Constants.DELETE, "payments." + stringMap.get(entityClass));
                preparedStatement = connection.prepareStatement(hallSQL);
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();
                break;

            case PAYMENT:

                preparedStatement = connection.prepareStatement("DELETE FROM payment where payment_id=?");
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();

                String sessionSQL = String.format(Constants.DELETE, "payments." + stringMap.get(entityClass));
                preparedStatement = connection.prepareStatement(sessionSQL);
                preparedStatement.setInt(1, (Integer) id);
                preparedStatement.executeUpdate();
                break;
        }
    }

    private void saveCreditCard(CreditCard creditCard) {
        Connection connection = dataSource.getConnection();
        try {
            if (creditCard.getClient().getId() == -1) {
                saveClient(creditCard.getClient());
            }
            Set<Payment> payments = creditCard.getPayments();
            if (payments.size() > 0) {
                for (Payment payment : payments) {

                    String paymentSQL = String.format(Constants.INSERT, "creditcard", " (client_id, cardopendate, payment_id) values (?,?,?)");
                    PreparedStatement preparedStatement = connection.prepareStatement(paymentSQL, Statement.RETURN_GENERATED_KEYS);
                    preparedStatement.setInt(1, creditCard.getClient().getId());
                    preparedStatement.setDate(2, new java.sql.Date(creditCard.getCardOpenDate().getTime()));
                    preparedStatement.setInt(3, payment.getId());
                    preparedStatement.executeUpdate();
                    ResultSet rs = preparedStatement.getGeneratedKeys();
                    rs.next();
                    creditCard.setId(rs.getInt(1));
                }
            } else {
                String paymentSQL = String.format(Constants.INSERT, "creditcard", " (client_id, cardopendate, payment_id) values (?,?,?)");
                PreparedStatement preparedStatement = connection.prepareStatement(paymentSQL, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, creditCard.getClient().getId());
                preparedStatement.setDate(2, new java.sql.Date(creditCard.getCardOpenDate().getTime()));
                preparedStatement.setNull(3, java.sql.Types.INTEGER);
                preparedStatement.executeUpdate();
                ResultSet rs = preparedStatement.getGeneratedKeys();
                rs.next();
                creditCard.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

//    private void updateHall(Hall hall) {
//        Connection connection = dataSource.getConnection();
//        try {
//            String hallSQL = String.format(Constants.UPDATE, "hall", " (name=?, rowCount=?, columnCount=?, size=?)");
//            PreparedStatement preparedStatement = connection.prepareStatement(hallSQL);
//            preparedStatement.setString(1, hall.getName());
//            preparedStatement.setInt(2, hall.getRowCount());
//            preparedStatement.setInt(3, hall.getColumnCount());
//            preparedStatement.setInt(4, hall.getSize());
//            preparedStatement.setInt(5, hall.getId());
//            preparedStatement.executeUpdate();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }finally {
//            try {
//                connection.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    private void saveClient(Client client) {
        Connection connection = dataSource.getConnection();
        try {
            String movieSQL = String.format(Constants.INSERT, "client", " (firstName, lastName, login, password, birthday, role, email) values (?,?,?,?,?,?,?)");
            PreparedStatement preparedStatement = connection.prepareStatement(movieSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, client.getFirstName());
            preparedStatement.setString(2, client.getLastName());
            preparedStatement.setString(3, client.getLogin());
            preparedStatement.setString(4, client.getPassword());
            preparedStatement.setDate(5, new java.sql.Date(client.getBirthDate().getTime()));
            preparedStatement.setString(6, client.getRole().name());
            preparedStatement.setString(7, client.getEmail());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            client.setId(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateClient(Client client) {
        Connection connection = dataSource.getConnection();
        try {
            String movieSQL = String.format(Constants.UPDATE, "client", " (firstName=?, lastName=?, login=?, password=?, birthday=?, role=?, email=?)");
            PreparedStatement preparedStatement = connection.prepareStatement(movieSQL);
            preparedStatement.setString(1, client.getFirstName());
            preparedStatement.setString(2, client.getLastName());
            preparedStatement.setString(3, client.getLogin());
            preparedStatement.setString(4, client.getPassword());
            preparedStatement.setDate(5, new java.sql.Date(client.getBirthDate().getTime()));
            preparedStatement.setString(6, client.getRole().name());
            preparedStatement.setString(7, client.getEmail());
            preparedStatement.setInt(8, client.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected List<T> readAll(ResultSet resultSet) throws SQLException {
        List<T> result = new LinkedList<>();

        while (resultSet.next()) {
            switch (stringMap.get(entityClass)) {
                case CLIENT:
                    Client client = mapClient(resultSet);
                    result.add((T) client);
                    break;
                case ACCOUNT:
                    Account account = mapAccount(resultSet);
                    result.add((T) account);
                    break;
                case CREDITCARD:
                    CreditCard creditCard = mapCreditCard(resultSet);
                    result.add((T) creditCard);
                    break;
                case PAYMENT:
                    Payment session = mapPayment(resultSet);
                    result.add((T) session);
                    break;
            }
        }
        return result;
    }

    private T readOne(ResultSet resultSet) throws SQLException {
        T result = null;
        while (resultSet.next()) {
            switch (stringMap.get(entityClass)) {
                case CLIENT:
                    Client client = mapClient(resultSet);
                    result = (T) client;
                    break;
                case ACCOUNT:
                    Account account = mapAccount(resultSet);
                    result = (T) account;
                    break;
                case CREDITCARD:
                    CreditCard creditCard = mapCreditCard(resultSet);
                    result = (T) creditCard;
                    break;
                case PAYMENT:
                    Payment payment = mapPayment(resultSet);
                    result = (T) payment;
                    break;
            }
        }
        return result;
    }

    protected Client mapClient(ResultSet resultSet) throws SQLException {
        Client movie = null;
        if (resultSet.getRow() > 0) {
            movie = new Client();
            movie.setId(resultSet.getInt("id"));
            movie.setFirstName(resultSet.getString("firstName"));
            movie.setLastName(resultSet.getString("lastName"));
            movie.setLogin(resultSet.getString("login"));
            movie.setPassword(resultSet.getString("password"));
            movie.setBirthDate(resultSet.getDate("birthday"));
            movie.setRole(RoleEnum.valueOf(resultSet.getString(("role"))));
            movie.setEmail(resultSet.getString("email"));
        }
        return movie;
    }

    protected Account mapAccount(ResultSet resultSet) throws SQLException {
        Account account = null;
        if (resultSet.getRow() > 0) {
            account = new Account(loadClientById(resultSet.getInt("client_id")));
            account.setId(resultSet.getInt("id"));
            account.setCreationDate(resultSet.getDate("creationdate"));
//            account.setCreditCardList(loadCreditCardById(resultSet.getString("creditcard_id")));
        }
        return account;
    }

    protected CreditCard mapCreditCard(ResultSet resultSet) throws SQLException {
        CreditCard creditCard = null;
        if (resultSet.getRow() > 0) {
            creditCard = new CreditCard(loadClientById(resultSet.getInt("client_id")));
            creditCard.setId(resultSet.getInt("id"));
            creditCard.setCardOpenDate(resultSet.getDate("cardopendate"));
//            creditCard.setPayments(loadPaymentById(resultSet.getInt("payment_id")));

        }
        return creditCard;
    }

    protected Payment mapPayment(ResultSet resultSet) throws SQLException {
        Payment payment = new Payment();
        payment.setId(resultSet.getInt("id"));
        payment.setCreditCard(loadCreditCardById(resultSet.getInt("creditcard_id")));
        payment.setOperationName(OperationNames.valueOf(resultSet.getString("operationname")));
        payment.setOperationDate(resultSet.getTimestamp("operationdate"));
        payment.setOperationDetails(resultSet.getString("operationdetails"));
        return payment;
    }

    private CreditCard loadCreditCardById(int creditCard_id) {
        String sql = String.format(Constants.FIND_BY_ID, "payments.creditcard");
        Connection connection = dataSource.getConnection();
        CreditCard result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, creditCard_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = mapCreditCard(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private Client loadClientById(int client_id) {
        String sql = String.format(Constants.FIND_BY_ID, "payments.client");
        Connection connection = dataSource.getConnection();
        Client result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, client_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = mapClient(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private Payment loadPaymentById(int payment_id) {
        String sql = String.format(Constants.FIND_BY_ID, "payments.payment");
        Connection connection = dataSource.getConnection();
        Payment result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, payment_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = mapPayment(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
