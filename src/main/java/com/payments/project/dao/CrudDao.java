package com.payments.project.dao;

import java.util.List;

/**
 * Created by natalia on 21.08.17.
 */
public interface CrudDao<T, S> {

    List<T> getAll();
    void create(T type);
    void update(T type);
    void delete(S id);
    T get(S id);

}
