package ut.payments.project;

import com.payments.project.dto.AccountDTO;
import com.payments.project.entity.Account;
import com.payments.project.entity.Client;
import com.payments.project.services.AccountService;
import com.payments.project.services.impl.AccountServiceImpl;
import com.payments.project.transformer.AccountTransformer;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by natalia on 28.08.17.
 */
public class AccountTests {
    @Test
    public void accountsEquality() throws ParseException {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
        AccountService accountService = new AccountServiceImpl();
        Client client1 = new Client("Ali", "Aliev", dateformat.parse("17/07/1989"), "ali", "ali", "ali@Aliev.com");
        Account account1 = new Account(client1);
        final AccountDTO accountDTO1 = AccountTransformer.accountToDto(account1);
        accountService.create(accountDTO1);

        Client client2 = new Client("megan", "Fox", dateformat.parse("01/12/1988"), "megan", "megan", "megan@fox.com");
        Account account2 = new Account(client2);
        final AccountDTO accountDTO2 = AccountTransformer.accountToDto(account2);
        accountService.create(accountDTO2);

        List<AccountDTO> expected = new ArrayList<AccountDTO>(){{
            add(accountDTO1);
            add(accountDTO2);
        }};

       List<AccountDTO> actual = accountService.getAll();

        assertEquals(expected, actual);

    }
}
